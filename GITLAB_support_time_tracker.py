from datetime import timedelta,datetime
from functions import *

#Defining the Gitlab credentials 
ACCESS_TOKEN='glpat-xuLSxZBG2d2yws4L4EBu'
EXPERIMENTATION_SUBJECT_ID=''
GROUP_NAME="rushdi-testing-org"
getProjects_URL = "https://gitlab.com/api/v4/groups/" + GROUP_NAME + "/projects"

README_FILENAME="README.md"
SUPPORT_FILENAME="support-utilization.md"

#local csv file
FILE_NAME="purchase_CSV.csv"


#reading the csv file
readCSV=readCSV(FILE_NAME)
header     = readCSV[0]
projectIds = readCSV[1]
purchases  = readCSV[2]

#defining the header index
PurchasedID=header.index("PurchasedID")
ProjectID=header.index("ProjectId")
PurchasedHOURS=header.index("PurchasedHours")
PurchasedON=header.index("PurchasedOn")
ExpiresON=header.index("ExpiresOn")

#Get all the project Ids
for pid in projectIds:
    #Get project id
    project_id=pid
    #Get all the issues based on the project id 
    issue_data=getProjectIssues(project_id, ACCESS_TOKEN, EXPERIMENTATION_SUBJECT_ID)
    
     #check if the project is available or not (Gitlab)
    try:
        
        #check if the project is available or not (Local file)
        if issue_data=="File not accessible": 
            continue
            
        utilization=""
        SUPPORT_UTILIZATION_FILE=SUPPORT_FILENAME
        total_project_hrs_purchased=0 #count purchase
        total_used_hrs=0 #count
        total_remaining_hrs=0 #count
        hours_to_next_purchasing=0
        start_issue_count=0

        c=0
        purchase_id=1
        for purchase in purchases:
            purchasedId = purchase[PurchasedID]
            ProjectId = purchase[ProjectID]
            PurchasedHours = purchase[PurchasedHOURS]
            PurchasedOn = purchase[PurchasedON]
            expiresOn = purchase[ExpiresON]

            if(ProjectId!=pid):
                continue
            if PurchasedHours.isnumeric()==False:
                PurchasedHours=0
            
            #print(purchasedId)
            #print (expiresOn)
            utilization+="# Support Hours Purchasing - "+str(purchase_id)+"\n"
            utilization+="| Hours Purchased | Putchased On | Expires On |\n"
            utilization+="| ------ | ------ | ------ |\n"
            utilization+="| "+str(PurchasedHours)+" | "+PurchasedOn+" | "+expiresOn+" |\n\n"
            utilization+="| Issue Id | Logger | Spent Time | Date | Note |\n"
            utilization+="| ------ | ------ | ------ | ------ | ------ |\n"
            
            if(c==0):
                sp=issue_data[0]['web_url'].split('-/')
                SUPPORT_UTILIZATION_FILE=sp[0]+"-/blob/main/"+SUPPORT_UTILIZATION_FILE
                web_url=issue_data[0]['web_url']
                sp1=web_url.split(GROUP_NAME+'/')
                p=sp1[1].split('/')
                PROJECT_NAME=p[0]
            used_hrs=0
            remaining_hrs=0
            for issue in issue_data[start_issue_count:]:
                issue_time_spent=issue['time_stats']['total_time_spent']
                ica=issue['created_at'].split('T')
                iid=issue['iid']
                author=issue['author']['name']
                itime_spent=((issue_time_spent)/(60*60))
                icreated_at=ica[0]
                
                if(icreated_at>expiresOn):
                    break
                    
                total_used_hrs+=itime_spent
                used_hrs+=itime_spent
                remaining_hrs=int(PurchasedHours)- used_hrs
                View="[View]("+web_url+")"
                utilization+="| "+str(iid)+" | "+author+" | "+str(round(itime_spent,1))+" | "+str(icreated_at)+" | "+View+" |\n"
    
                start_issue_count+=1
            total_project_hrs_purchased+=int(PurchasedHours)
            total_remaining_hrs=int(PurchasedHours)-used_hrs   
            c+=1
            purchase_id+=1
            utilization+="\n"
    except:
        print(issue_data['message'])#if not found 
        break
    
    if(total_remaining_hrs<0):
            utilization+="| Unpaid Support Hours |\n"
            utilization+="| ------ |\n"
            utilization+="| "+str((-1)*total_remaining_hrs)+" |\n\n"
            total_remaining_hrs=0
            
    readme_data = {"PROJECT_NAME": PROJECT_NAME, 'SUPPORT_UTILIZATION_FILE': SUPPORT_UTILIZATION_FILE,
                  "TOTAL_USED_HOURS": total_used_hrs, 'TOTAL_REMAINING_HOURS': total_remaining_hrs,
                  "TOTAL_PROJECT_HOURS_PURCHASED": total_project_hrs_purchased, 'EXPIRESON': expiresOn}

    readme = CreateReadMEFile(readme_data)
    print(putToFile(project_id, ACCESS_TOKEN, EXPERIMENTATION_SUBJECT_ID, README_FILENAME, readme))
    print(putToFile(project_id, ACCESS_TOKEN, EXPERIMENTATION_SUBJECT_ID, SUPPORT_FILENAME, utilization))