# Support Hours Purchasing - 2
| Hours Purchased | Putchased On | Expires On |
| ------ | ------ | ------ |
| 5 | 2021-09-01 | 2021-09-30 |

| Issue Id | Logger | Spent Time | Date | Note |
| ------ | ------ | ------ | ------ | ------ |

# Support Hours Purchasing - 4
| Hours Purchased | Putchased On | Expires On |
| ------ | ------ | ------ |
| 15 | 2021-10-01 | 2021-10-29 |

| Issue Id | Logger | Spent Time | Date | Note |
| ------ | ------ | ------ | ------ | ------ |

# Support Hours Purchasing - 5
| Hours Purchased | Putchased On | Expires On |
| ------ | ------ | ------ |
| 9 | 2021-11-01 | 2021-11-29 |

| Issue Id | Logger | Spent Time | Date | Note |
| ------ | ------ | ------ | ------ | ------ |
| 1 | Rushdi Mohamed | 1.8 | 2021-11-15 | [View](https://gitlab.com/rushdi-testing-org/testing-1/-/issues/1) |
| 2 | Rushdi Mohamed | 0.7 | 2021-11-15 | [View](https://gitlab.com/rushdi-testing-org/testing-1/-/issues/1) |
| 3 | Rushdi Mohamed | 1.3 | 2021-11-15 | [View](https://gitlab.com/rushdi-testing-org/testing-1/-/issues/1) |

